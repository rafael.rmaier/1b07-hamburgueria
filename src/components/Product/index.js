

export const Product = ({product, eventHandler}) => {
     return (
     <div className="productOnMenu" key={product.Id}>
            <h1 className="productName">{product.name}</h1>
            <p className="productCategory">{product.category}</p>
            <p className="productPrice">{product.price}</p>
            <button onClick={() => eventHandler(product)}>Adicionar ao carrinho</button>
    </div>
    )
}