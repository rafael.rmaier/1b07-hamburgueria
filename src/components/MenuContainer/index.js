import {Product} from '../../components/Product';

export const MenuContainer = ({productArray, eventHandler, emptySearch}) => {


    return(
        <div className="productsMenu">
            {emptySearch ? (
                <div>Não existem resultados</div>
            ):(
            productArray.map((product) => (
               
            <Product key={product.id} product={product} eventHandler={eventHandler}/>

                
            )))}
        </div>
    )
}