import logo from './logo.svg';
import './App.css';
import {useState} from 'react';
import {MenuContainer} from './components/MenuContainer';

function App() {

  const [products, setProducts] = useState([
    { id: 1, name: 'Hamburguer', category: 'Sanduíches', price: 7.99 },
    { id: 2, name: 'X-Burguer', category: 'Sanduíches', price: 8.99 },
    { id: 3, name: 'X-Salada', category: 'Sanduíches', price: 10.99 },
    { id: 4, name: 'Big Kenzie', category: 'Sanduíches', price: 16.99 },
    { id: 5, name: 'Guaraná', category: 'Bebidas', price: 4.99 },
    { id: 6, name: 'Coca', category: 'Bebidas', price: 4.99 },
    { id: 7, name: 'Fanta', category: 'Bebidas', price: 4.99 },
  ]); 

  const [emptySearch,setEmptySearch] = useState(false);

  const [filteredProducts, setFilteredProducts] = useState([...products]);

  const showProducts = (input) => {
  setFilteredProducts(products.filter((product) => 
  product.name.toLowerCase().includes(input.toLowerCase()) || 
  product.category.toLowerCase().includes(input.toLowerCase())));

  if(filteredProducts.length === 0){
    setEmptySearch(true);
  }

  }

  const verifyInputIsEmpty = (input) => {
    if(input.length === 0) setEmptySearch(false);
  }

  const handleClick = (product) => {
    setCurrentSale({...currentSale, total: currentSale.total + product.price, saleDetails: [...currentSale.saleDetails, product]});
  } 

 
  const showSale = (currentSale) => {
    return currentSale.saleDetails.map((product) => 

      (<div className="currentProduct">
            <h1 className="productName">{product.name}</h1>
            <p className="productCategory">{product.category}</p>
            <p className="productPrice">{product.price}</p>
      </div>)
    )
  }
  const round = (number) => {
    return Math.round((number + Number.EPSILON) * 100) / 100;
  }


  const [currentSale, setCurrentSale] = useState({total: 0, saleDetails: []});

  const [inputValue, setInputValue] = useState('');
  return (
    <div className="App">
      <div className="header">
        <h1 className="restaurantName">WILD WEST BURGUERS</h1>
      <div className="filter">
          <input 
            className="productSearch"
            type='text'
            placeholder="Watchya looking for, cowboy?"
            value={inputValue}
            onChange={(event) => {
              setInputValue(event.target.value)
              showProducts(event.target.value)
              verifyInputIsEmpty(event.target.value)
              }
              } />

          
        </div>
      </div>
      <div className="main">
        <h2 className="menu">MENU</h2>
        
        <MenuContainer productArray={filteredProducts} eventHandler={handleClick} emptySearch={emptySearch } />
      <hr></hr>
        {(!emptySearch &&
        <div className="currentSale">
           <div className="subtotal"><p >Subtotal: </p> {round((currentSale.saleDetails.reduce((acc, product) => {
            return acc + product.price}, 0)))}
           </div>
            {showSale(currentSale)}
        </div>)
        }

      </div>
        

        
        

    </div>
    )
  
}

export default App;
